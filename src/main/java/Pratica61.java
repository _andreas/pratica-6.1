
/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática IF62C - Fundamentos de Programação 2
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

public class Pratica61 {

    public static void main(String[] args) {
        
        Time time1 = new Time();
        Time time2 = new Time();
        
        time1.addJogador("Goleiro", new Jogador(1, "Fulano"));
        time1.addJogador("Lateral", new Jogador(4, "Ciclano"));
        time1.addJogador("Atacante", new Jogador(10, "Beltrano"));
       
        time2.addJogador("Goleiro", new Jogador(1, "João"));
        time2.addJogador("Lateral", new Jogador(7, "José"));
        time2.addJogador("Atacante", new Jogador(15, "Mário"));

        System.out.println("           time 1" + "          time 2");
        for (String posicoes : time1.getJogadores().keySet()) {
            System.out.println(posicoes + " " + time1.getJogadores().get(posicoes) + " / " + time2.getJogadores().get(posicoes));
        }

    }
}
